pg-d2
=================


<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g pg-d2
$ pg-d2 COMMAND
running command...
$ pg-d2 (--version)
pg-d2/0.1.0 linux-x64 node-v18.15.0
$ pg-d2 --help [COMMAND]
USAGE
  $ pg-d2 COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`pg-d2 create-config FILE`](#pg-d2-create-config-file)
* [`pg-d2 generate [OUTPUTFILE]`](#pg-d2-generate-outputfile)
* [`pg-d2 help [COMMANDS]`](#pg-d2-help-commands)

## `pg-d2 create-config FILE`

creates a new configuration file with sensible defaults.

```
USAGE
  $ pg-d2 create-config FILE [-f]

ARGUMENTS
  FILE  path to the file to be created.

FLAGS
  -f, --force  set to overwrite a file if one already exists.

DESCRIPTION
  creates a new configuration file with sensible defaults.

EXAMPLES
  $ pg-d2 create-config pg-d2.config.json
```

_See code: [src/commands/create-config.ts](https://github.com/martmaemees/pg-d2/blob/v0.1.0/src/commands/create-config.ts)_

## `pg-d2 generate [OUTPUTFILE]`

Generate D2 diagram file from a running PostgreSQL database.

```
USAGE
  $ pg-d2 generate [OUTPUTFILE] [-c <value>] [-h <value>] [-p <value>] [-U <value>] [-P <value>] [-d <value>]
    [-s <value>] [-f] [--debug]

ARGUMENTS
  OUTPUTFILE  path to output the D2 diagram file to.

FLAGS
  -P, --pass=<value>      Password of the database user.
  -U, --user=<value>      Database user to use.
  -c, --config=<value>    [default: pg-d2.config.json] Path to the optional configuration file. Configuration file will
                          override defaults. Command line properties will override configuration file.
  -d, --database=<value>  Name of the database to generate the diagram from.
  -f, --force             Set to force the application to overwrite the file at outputFile, if one already exists on the
                          path.
  -h, --host=<value>      Hostname of the database.
  -p, --port=<value>      Port of the database.
  -s, --schema=<value>    Schema of the database to generate a diagram from. (Only one schema at once is supported.)
      --debug             Log complete configuration.

DESCRIPTION
  Generate D2 diagram file from a running PostgreSQL database.

EXAMPLES
  $ pg-d2 generate out.d2 --host localhost --port 5432 --user testuser --pass testpass --database testdb

  $ pg-d2 generate -c public-schema.config.json
```

_See code: [src/commands/generate.ts](https://github.com/martmaemees/pg-d2/blob/v0.1.0/src/commands/generate.ts)_

## `pg-d2 help [COMMANDS]`

Display help for pg-d2.

```
USAGE
  $ pg-d2 help [COMMANDS] [-n]

ARGUMENTS
  COMMANDS  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for pg-d2.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v5.2.20/src/commands/help.ts)_
<!-- commandsstop -->
