create database test;
GO

use test;

create table person_address (
    id bigint not null identity(1, 1) primary key,

    country varchar not null,
    city varchar not null,
    street_name varchar not null,
    house_nr varchar not null,
    apartment_nr varchar
);

create table person (
    id bigint not null identity(1, 1) primary key,
    first_name varchar not null,
    last_name varchar not null,

    phone_nr integer,
    address_id bigint references person_address(id) on update cascade on delete cascade
);
GO
