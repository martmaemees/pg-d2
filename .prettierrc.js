import oclifConfig from "@oclif/prettier-config"

export default {
  ...oclifConfig,
  semi: true,
  lineWidth: 140,
  singleQuote: true,
  tabWidth: 2,
  trailingComma: 'es5'
}
