import {Args, Command, Flags} from '@oclif/core';
import {Listr} from 'listr2';
import {merge} from 'lodash-es';
import {ZodError} from 'zod';

import {GenerateConfig, GenerateConfigSchema} from '../lib/config.model.js';
import DatabaseInspector from '../lib/inspector.js';
import {Table} from '../lib/model.js';
import {isError, parseFile, writeFile} from '../lib/util.js';

const RESERVED_NAMES = new Set(['label', 'shape']);

const cleanReservedNames = (str: string) => {
  if (RESERVED_NAMES.has(str)) {
    return `${str}_`;
  }

  return str;
};

interface Ctx {
  inspector: DatabaseInspector;
  tables?: Record<string, Table>;
  config: GenerateConfig;
  res: string;
  cmd: Generate;
}

const defaultConfigJson = 'pg-d2.config.json';
const defaultConfigYaml = 'pg-d2.config.yaml';
const defaultConfigYml = 'pg-d2.config.yml';

export default class Generate extends Command {
  static args = {
    outputFile: Args.string({description: 'path to output the D2 diagram file to.'}),
  };

  static description = 'Generate D2 diagram file from a running PostgreSQL database.';
  static examples = [
    `$ pg-d2 generate out.d2 --host localhost --port 5432 --user testuser --pass testpass --database testdb`,
    '$ pg-d2 generate -c public-schema.config.json',
  ];

  static flags = {
    config: Flags.string({
      char: 'c',
      default: 'pg-d2.config.json',
      description:
        'Path to the optional configuration file. Configuration file will override defaults. Command line properties will override configuration file.',
    }),

    host: Flags.string({char: 'h', description: 'Hostname of the database.'}),
    port: Flags.integer({char: 'p', description: 'Port of the database.'}),
    user: Flags.string({char: 'U', description: 'Database user to use.'}),
    pass: Flags.string({char: 'P', description: 'Password of the database user.'}),

    database: Flags.string({char: 'd', description: 'Name of the database to generate the diagram from.'}),
    schema: Flags.string({
      char: 's',
      description: 'Schema of the database to generate a diagram from. (Only one schema at once is supported.)',
    }),

    force: Flags.boolean({
      char: 'f',
      description:
        'Set to force the application to overwrite the file at outputFile, if one already exists on the path.',
    }),

    debug: Flags.boolean({
      description: 'Log complete configuration.',
    }),
  };

  async parseConfigFile<T>(path: string, fileNotExistCallback?: () => Promise<T | undefined>): Promise<T | undefined> {
    try {
      return await parseFile(path);
    } catch (error) {
      if (isError(error) && error.code === 'ENOENT') {
        if (fileNotExistCallback !== undefined) {
          return fileNotExistCallback();
        }

        this.debug(`Configuration file ${path} does not exist.`);
        return undefined;
      }

      this.error(error as Error);
    }
  }

  public async run(): Promise<void> {
    const {
      args: {outputFile},
      flags,
      metadata,
    } = await this.parse(Generate);

    let configFile;
    if (metadata?.flags?.config?.setFromDefault) {
      configFile = await this.parseConfigFile<GenerateConfig>(defaultConfigJson, async () =>
        this.parseConfigFile<GenerateConfig>(defaultConfigYaml, async () =>
          this.parseConfigFile<GenerateConfig>(defaultConfigYml)
        )
      );
    } else {
      configFile = await this.parseConfigFile<GenerateConfig>(flags.config);
    }

    const mergedConfig = merge(configFile, {
      connection: {
        host: flags.host,
        port: flags.port,
        user: flags.user,
        pass: flags.pass,
        database: flags.database,
      },
      force: flags.force,
      outputFile,
      schema: flags.schema,
    });

    let config;
    try {
      config = await GenerateConfigSchema.parseAsync(mergedConfig);
    } catch (error) {
      if (error instanceof ZodError) {
        let errString = 'Found problems with the configuration:\n';
        errString += error.issues.map((issue) => ` * [${issue.path.join('.')}] : ${issue.message}`).join('\n');
        this.error(errString);
      } else {
        this.error(error as Error);
      }
    }

    if (flags.debug) {
      console.dir(config, {depth: 3});
    }

    const ctx: Ctx = {
      inspector: new DatabaseInspector(config.connection),
      config,
      res: '',
      cmd: this,
    };

    const tasks = new Listr<Ctx>([
      {
        title: 'Database inspection',
        task: async () =>
          new Listr([
            {
              title: `Connecting to the database ${ctx.config.connection.database}`,
              async task(ctx) {
                try {
                  await ctx.inspector.connect();
                } catch (error) {
                  ctx.inspector.close();
                  throw error;
                }
              },
            },
            {
              title: `Inspecting the database schema ${ctx.config.schema}`,
              async task(ctx) {
                try {
                  ctx.tables = await ctx.inspector.getAllTablesOfSchema(ctx.config.schema);
                  console.dir(ctx.tables, { depth: 5 });
                  // ctx.cmd.error("WELP");
                  ctx.tables = await ctx.inspector.fillAllColumnsOfSchema(ctx.config.schema, ctx.tables);
                  console.dir(ctx.tables, { depth: 5 });
                  ctx.tables = await ctx.inspector.fillAllConstraintsOfSchema(ctx.config.schema, ctx.tables);
                  console.dir(ctx.tables, { depth: 5 });
                  ctx.tables = await ctx.inspector.fillAllForeignKeysofSchema(ctx.config.schema, ctx.tables);
                  console.dir(ctx.tables, { depth: 5 });
                } finally {
                  await ctx.inspector.close();
                }
              },
            },
          ]),
      },

      {
        title: 'Building the D2 diagram structure',
        task(ctx) {
          const tableToContainer: Record<string, string> = {};

          if (ctx.config.tables) {
            for (const [container, tables] of Object.entries(ctx.config.tables)) {
              for (const table of Object.keys(tables)) {
                if (Object.hasOwn(tableToContainer, table)) {
                  ctx.cmd.error(`The tables configuration defined table '${table}' multiple times!`);
                }

                tableToContainer[table] = container;
              }
            }
          }

          function getContainer(table: string): string {
            let container = tableToContainer[table] ?? "";
            if (container.length > 0) {
              container += ".";
            }

            return container;
          }

          for (const table of Object.values(ctx.tables!)) {
            const container = getContainer(table.name);
            const tooltip = Object.hasOwn(tableToContainer, table.name) ? ctx.config.tables?.[tableToContainer[table.name]]?.[table.name]?.description : null;

            ctx.res += `${container}${cleanReservedNames(table.name)}: {\n\tshape: sql_table\n`;
            if (tooltip) {
              ctx.res += `\ttooltip: ${tooltip}\n`;
            }
            
            ctx.res += "\n";

            for (const column of Object.values(table.columns)) {
              ctx.res += `\t${cleanReservedNames(column.name)}: ${column.dataType}`;
              const constraints = Object.values(table.constraints)
                .filter((c) => c.columns.length === 1 && c.columns[0] === column.name)
                .map((c) => c.type)
                .map((t) => t.toLowerCase().replaceAll(' ', '_'));

              if (
                Object.values(table.foreignKeys).some(
                  (key) => key.sourceTable === table.name && key.sourceColumn === column.name
                )
              ) {
                constraints.push('foreign_key');
              }

              if (constraints.length > 0) {
                ctx.res += ` { constraint: [ ${constraints.join('; ')} ] }`;
              }

              ctx.res += '\n';
            }

            ctx.res += '}\n';

            for (const fkey of Object.values(table.foreignKeys)) {
              ctx.res += `${container}${cleanReservedNames(fkey.sourceTable)}.${fkey.sourceColumn} -> ${getContainer(fkey.targetTable)}${cleanReservedNames(
                fkey.targetTable
              )}.${fkey.targetColumn}\n`;
            }

            ctx.res += '\n';
          }
        },
      },

      {
        title: 'Writing the D2 diagram to the output file',
        async task(ctx) {
          await writeFile(ctx.config.outputFile, ctx.res, ctx.config.force, ctx.cmd.error);
        },
      },
    ]);

    await tasks.run(ctx);
  }
}
