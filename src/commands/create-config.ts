/* eslint-disable perfectionist/sort-classes */
import {Args, Command, Flags} from '@oclif/core'
import { Listr } from 'listr2';
import * as yaml from 'yaml';

import { GenerateConfig } from '../lib/config.model.js';
import { writeFile } from '../lib/util.js';

const defaultConfig: GenerateConfig = {
  connection: {
    host: "localhost",
    port: 5432,
    user: "postgres",
    pass: "postgres",
    database: "database",
    dbms: "postgres"
  },
  force: false,
  outputFile: "pg-d2.config.json",
  schema: "public",
  tables: {
    "": {
      "your_table": {
        description: "Description of what the table represents."
      }
    },
    "Group 1": {
      "second_table": {
        description: "This table will be placed in its own group in the resulting di"
      }
    }
  }
};

export default class CreateConfig extends Command {
  static description = 'creates a new configuration file with sensible defaults.';

  static examples = [
    '<%= config.bin %> <%= command.id %> pg-d2.config.json',
  ]

  static flags = {
    // flag with no value (-f, --force)
    force: Flags.boolean({char: 'f', default: false, description: 'set to overwrite a file if one already exists.'}),
  }

  static args = {
    file: Args.string({description: 'path to the file to be created.', required: true}),
  }

  public async run(): Promise<void> {
    const {args, flags} = await this.parse(CreateConfig);

    const tasks = new Listr<{cmd: CreateConfig}>([
      {
        title: 'Write new configuration file.',
        async task(ctx) {
          let content = '';
          if (args.file.endsWith(".yaml") || args.file.endsWith(".yml")) {
            content = yaml.stringify(defaultConfig, { indent: 2 });
          } else if (args.file.endsWith(".json")) {
            content = JSON.stringify(defaultConfig, undefined, 2)
          } else {
            ctx.cmd.error('File must have the extension of ".json", ".yaml" or ".yml".');
          }

          await writeFile(args.file, content, flags.force, ctx.cmd.error)
        }
      }
    ]);

    await tasks.run({
      cmd: this
    });
  }
}
