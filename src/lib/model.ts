export interface Table {
    name: string;
    columns: Record<string, Column>;
    constraints: Record<string, Constraint>;
    foreignKeys: Record<string, ForeignKey>;
}

export interface Column {
    name: string;
    dataType: string;
    isNullable: boolean;
    ordinalPosition: number;
    
    default?: string;
    charMaxLength?: number;
    numPrecision?: number;
    isIdentity?: boolean;
    isGenerated?: boolean;
    isUpdatable?: boolean;

    isUnique?: boolean;
}

export interface Constraint {
    name: string;
    type: string;
    columns: string[];
}

export interface ForeignKey {
  name: string;

  sourceTable: string;
  sourceColumn: string;

  targetTable: string;
  targetColumn: string;
}
