import * as fs from 'node:fs/promises';
import * as yaml from 'yaml';

export function isError(error: unknown): error is NodeJS.ErrnoException {
  return error instanceof Error;
}

export async function parseFile<T>(path: string): Promise<T> {
  let file;
  try {
    file = await fs.open(path, 'r');
    const fileString = await file.readFile({ encoding: 'utf8' });
    if (path.endsWith(".json")) {
      return await JSON.parse(fileString) as T;
    }
 
    if (path.endsWith(".yaml") || path.endsWith(".yml")) {
      return await yaml.parse(fileString) as T;
    }
 
    throw new Error('File must have the extension of ".json", ".yaml" or ".yml".');
  } finally {
    await file?.close();
  }
}

export async function writeFile(path: string, content: string, force: boolean, errorFn: (err: Error | string) => void): Promise<void> {
  let file;
  try {
    file = await fs.open(path, force ? 'w' : 'wx');
    await file.write(content, 0, 'utf8');
  } catch (error) {
    if (isError(error) && error.code === 'EEXIST') {
      errorFn(
        'The output file already exists. Turn on --force flag, if you wish to overwrite the existing file.'
      );
    }

    errorFn(error instanceof Error ? error : 'Something unexpected happened: ' + error);
  } finally {
    await file?.close();
  }
}
