import {Client} from 'ts-postgres';

import Db from './db/db.js';
import MSSQL from './db/mssql.js';
import PostgreSQL from './db/pg.js';
import {Table} from './model.js';

export interface DatabaseConnectionInfo {
  dbms: 'mssql'|'postgres';
  host: string;
  port: number;
  user: string;
  pass?: string;

  database: string;
}

// https://www.postgresql.org/docs/current/information-schema.html
export default class DatabaseInspector {
  db: Db;

  constructor(con: DatabaseConnectionInfo) {
    switch (con.dbms) {
      case 'postgres': {
        this.db = new PostgreSQL(con);
        break;
      }

      case 'mssql': { 
        this.db = new MSSQL(con);
        break;
      }

      default: {
        throw new Error(`Unsupported database selected: ${con.dbms}`);
      }
    }
  }

  async close(): Promise<void> {
    await this.db.close();
  }

  async connect(): Promise<void> {
    await this.db.connect();
  }

  async fillAllColumnsOfSchema(schema: string, tables: Record<string, Table>): Promise<Record<string, Table>> {
    const sqlQuery = this.db instanceof PostgreSQL 
      ? sql`
        select c.table_name, c.column_name, c.data_type, c.column_default, c.is_nullable, c.character_maximum_length, c.numeric_precision, c.is_identity, c.is_generated, c.is_updatable, c.ordinal_position, e.data_type as element_type 
        from information_schema.columns c
        left join information_schema.element_types e on (
            (c.table_catalog, c.table_schema, c.table_name, 'TABLE', c.dtd_identifier) = (e.object_catalog, e.object_schema, e.object_name, e.object_type, e.collection_type_identifier)
        ) 
        where table_schema = $1 order by table_name, ordinal_position asc;
      `
      : sql`
        select c.table_name, c.column_name, c.data_type, c.column_default, c.is_nullable, c.character_maximum_length, c.numeric_precision, c.ordinal_position
        from information_schema.columns c
        where table_schema = $1 order by table_name, ordinal_position asc
        ;
      `

    const result = await this.db.query(sqlQuery, [schema]);

    for (const row of result) {
      const table = tables[row.table_name];
      if (table === undefined) {
        continue;
      }

      let dataType = row.data_type;
      if (dataType === 'ARRAY') {
        dataType = `'${row.element_type}[]'`;
      }

      table.columns[row.column_name] = {
        name: row.column_name,
        dataType,
        isNullable: this.yesNo(row.is_nullable),
        ordinalPosition: row.ordinal_position,

        default: row.column_default,
        charMaxLength: row.character_maximum_length,
        numPrecision: row.numeric_precision,
        isIdentity: this.yesNo(row.is_identity),
        isGenerated: this.yesNo(row.is_generated),
        isUpdatable: this.yesNo(row.is_updatable),
      };
    }

    return tables;
  }

  async fillAllConstraintsOfSchema(schema: string, tables: Record<string, Table>): Promise<Record<string, Table>> {
    const result = await this.db.query(
      sql`
        select tc.table_name, tc.constraint_type, tc.constraint_name, kcu.column_name 
        from information_schema.table_constraints tc 
        join information_schema.key_column_usage kcu on kcu.constraint_name = tc.constraint_name and kcu.constraint_schema = tc.constraint_schema 
        where tc.constraint_type in ('PRIMARY KEY', 'UNIQUE') and tc.table_schema = $1;
      `,
      [schema]
    );

    for (const row of result) {
      const table = tables[row.table_name];
      if (table === undefined) {
        continue;
      }

      if (table.constraints[row.constraint_name] === undefined) {
        table.constraints[row.constraint_name] = {
          name: row.constraint_name,
          type: row.constraint_type,
          columns: [row.column_name],
        };
      } else {
        table.constraints[row.constraint_name].columns.push(row.column_name);
      }
    }

    return tables;
  }

  async fillAllForeignKeysofSchema(schema: string, tables: Record<string, Table>): Promise<Record<string, Table>> {
    const result = await this.db.query(
      // https://stackoverflow.com/a/3907999
      this.db instanceof MSSQL
        ? sql`
          SELECT 
            KCU1.CONSTRAINT_SCHEMA AS constraint_schema,
            KCU1.CONSTRAINT_NAME AS constraint_name,
            KCU1.TABLE_NAME AS table_name,
            KCU1.COLUMN_NAME AS column_name,
            KCU2.TABLE_NAME AS target_table,
            KCU2.COLUMN_NAME AS target_column 
          FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS RC 

          INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU1 
              ON KCU1.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG  
              AND KCU1.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA 
              AND KCU1.CONSTRAINT_NAME = RC.CONSTRAINT_NAME 

          INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU2 
              ON KCU2.CONSTRAINT_CATALOG = RC.UNIQUE_CONSTRAINT_CATALOG  
              AND KCU2.CONSTRAINT_SCHEMA = RC.UNIQUE_CONSTRAINT_SCHEMA 
              AND KCU2.CONSTRAINT_NAME = RC.UNIQUE_CONSTRAINT_NAME 
              AND KCU2.ORDINAL_POSITION = KCU1.ORDINAL_POSITION 
        `
        : sql`
          select tc.constraint_schema, tc.constraint_name, tc.table_name, kcu.column_name, ccu.table_name as target_table, ccu.column_name as target_column 
          from information_schema.table_constraints tc 
          join information_schema.key_column_usage kcu on tc.constraint_name = kcu.constraint_name and tc.constraint_schema = kcu.constraint_schema 
          join information_schema.constraint_column_usage ccu on ccu.constraint_name = tc.constraint_name and tc.table_schema = ccu.table_schema 
          where tc.constraint_type = 'FOREIGN KEY' and tc.constraint_schema = $1;
        `,
      [schema]
    );

    for (const row of result) {
      const table = tables[row.table_name];
      if (table === undefined) {
        continue;
      }

      if (table.foreignKeys[row.constraint_name] === undefined) {
        table.foreignKeys[row.constraint_name] = {
          name: row.constraint_name,
          sourceTable: row.table_name,
          sourceColumn: row.column_name,
          targetTable: row.target_table,
          targetColumn: row.target_column,
        };
      } else {
        // FIXME: Probably need to support multi column keys :)
      }
    }

    return tables;
  }

  async getAllTablesOfSchema(schema: string, excludedTables?: string[]): Promise<Record<string, Table>> {
    const result = await this.db.query(
      sql`select table_name from information_schema.tables where table_schema = $1 and table_type = 'BASE TABLE';`,
      [schema]
    );
    console.dir(result);
    const tables: Record<string, Table> = {};
    for (const row of result) {
      if (excludedTables !== undefined && !excludedTables.includes(row.table_name)) {
        continue;
      }

      tables[row.table_name] = {name: row.table_name, columns: {}, constraints: {}, foreignKeys: {}};
    }

    return tables;
  }

  private yesNo(input: string): boolean {
    switch (input) {
      case 'YES': {
        return true;
      }

      case 'NO': {
        return false;
      }

      default: {
        return false;
      }
    }
  }
}

// Dummy template tag for sql syntax highlighting extension.
// https://stackoverflow.com/a/63259137
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const sql = (strings: TemplateStringsArray, ...expr: any[]) =>
  strings.map((str, index) => str + (expr.length > index ? String(expr[index]) : '')).join('');
