import { Table } from "../model.js";

/* eslint-disable perfectionist/sort-union-types */
export default abstract class Db {
  abstract close(): Promise<void>;
  abstract connect(): Promise<void>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  abstract query(sql: string, params: (string|number|boolean|Date)[]): Promise<Record<string, any>[]>;
}
