// import { ConnectionPool, IRecordSet, connect, query,  } from 'mssql';
// eslint-disable-next-line import/default
import mssql from 'mssql';
const { ConnectionPool, NVarChar } = mssql;

import { DatabaseConnectionInfo } from '../inspector.js';
import Db from "./db.js";

export default class MSSQL extends Db {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  pool: any;

  constructor(con: DatabaseConnectionInfo) {
    super();
    this.pool = new ConnectionPool({
      server: con.host,
      port: con.port,
      user: con.user,
      password: con.pass,
      database: con.database,
      options: {
        trustServerCertificate: true
      }
    });
  }

  async close(): Promise<void> {
    await this.pool.close();
  }

  async connect(): Promise<void> {
    await this.pool.connect();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async query(sql: string, params: (Date | boolean | number | string)[]): Promise<Record<string, any>[]> {
    let req = this.pool.request();
    let i = 1;

    for (const param of params) {
      sql = sql.replaceAll(`$${i}`, `@${i}`);
      req = req.input(`${i}`, NVarChar, param);
      console.log(`Input [@${i}]: ${param}`);
      i++;
    }

    console.log(`SQL: ${sql}`);
    const res = await req.query(sql);
    console.dir(res, { depth: 5 });

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return res.recordset as any;
  }

}