import { Client } from "ts-postgres";

import { DatabaseConnectionInfo } from "../inspector.js";
import { Table } from "../model.js";
import Db from "./db.js";

export default class PostgreSQL extends Db {

  client: Client;

  constructor(con: DatabaseConnectionInfo) {
    super();
    this.client = new Client({
      host: con.host,
      port: con.port,
      user: con.user,
      password: con.pass,
      database: con.database,
    });
  }

  async close(): Promise<void> {
    await this.client.end();
  }

  async connect(): Promise<void> {
    await this.client.connect();
  }
  
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async query(sql: string, params: (Date | boolean | number | string)[]): Promise<Record<string, any>[]> {
    const res = await this.client.query(sql, params);
    console.dir(res);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return res as any;
  }
  
}